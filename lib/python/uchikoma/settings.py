import os, sys

################################################################################

import bulbs.neo4jserver

NeoGraphs = dict([
    (key, bulbs.neo4jserver.Graph(bulbs.neo4jserver.Config(os.environ['UCHIKOMA_NEO4J_'+key], os.environ['UCHIKOMA_NEO4J_USER'], os.environ['UCHIKOMA_NEO4J_PASS'])))
    for key in ('KINECT', 'KAPPA', 'RHO')
])

################################################################################

from pymongo import MongoClient

MongoStores = dict([
    (key, MongoClient(os.environ['UCHIKOMA_MONGO_'+key]))
    for key in ('KINECT', 'KAPPA', 'RHO')
])

################################################################################

import erppeek

ERP = erppeek.Client(os.environ['UCHIKOMA_BACKEND_ERP_EP'],
    db       = os.environ['UCHIKOMA_BACKEND_ERP_NAME'],
    user     = os.environ['UCHIKOMA_BACKEND_ERP_USER'],
    password = os.environ['UCHIKOMA_BACKEND_ERP_PASS'],
verbose=False)

################################################################################

from couchbase import Couchbase
from couchbase.exceptions import CouchbaseError

Couch = Couchbase.connect(bucket='default', host='localhost')
